package com.myntra.gdsStage;

import com.myntra.healthCheck.dao.Runs;
import com.myntra.healthCheck.dao.Services;
import com.myntra.utils.db.DBUtilities;
import com.myntra.utils.logger.ILogger;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;

import java.io.*;
import java.nio.file.StandardOpenOption;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class GdsUtils implements ILogger {

    /**
     * Get all the queries in each .sql file for doing the gds update
     * @param fr The file reader object of the corresponding db file which is curemtly read
     * @return Query list containing all the upadte queries which need to be executed
     * @throws IOException
     */
    public static List<String> getStrings(FileReader fr) throws IOException {
        List < String > queries = new ArrayList< String >();
        String read;
        BufferedReader br = new BufferedReader(fr);
        while ((read = br.readLine()) != null) {
            String[] query = read.split(";");
            for (String part: query) {
                queries.add(part);
            }
            LOG.debug(queries);
        }
        return queries;
    }


    /**
     * Insert the new record to 'Runs' table containing the details of the gds upadte run happend
     * @param startTime The time at which the gds update started
     * @param services The list of databases which was 12updated
     */
    public static void doRunsTableUpdate(String startTime, String services, String user) {
        /*List<Map<String, Object>> itemCount = DBUtilities.getDB("myntra").getJdbcTemplate()
                .queryForList("SELECT count(*) FROM Runs");
        System.out.println(itemCount.get(0).get("count(*)"));
        Long rowData = (Long) itemCount.get(0).get("count(*)");
        int rowCount = rowData.intValue();*/
        int rowCountValue = getRowCountFromTable("Runs");
        Runs runs = new Runs();
        runs.setId(++rowCountValue);
        runs.setStartTime(startTime);
        //runs.setCompleteTime(completedTime);
        runs.setServices(services);
        runs.setUser(user);
        int listOfDrops = DBUtilities.getDB("myntra").getJdbcTemplate()
                .update("INSERT INTO Runs(id, User, Services, StartedOn) VALUES (?, ?, ?, ?)", new Object[]{runs.getId(), runs.getUser(), runs.getServices(), runs.getStartTime()});

        if (listOfDrops == 0) {
            LOG.error("\n\nUpdate failed for the db:- Runs. Query\n");
            LOG.error("Doing a retry");
            listOfDrops = DBUtilities.getDB("myntra").getJdbcTemplate()
                    .update("INSERT INTO Runs(id, User, Services, StartedOn) VALUES (?, ?, ?, ?)", new Object[]{runs.getId(), runs.getUser(), runs.getServices(), runs.getStartTime()});
        }
        /*DBUtilities.getDB("myntra").getJdbcTemplate()
                .update("UPDATE Services set runId = ?", new Object[]{runs.getId()});*/

    }





    public static void doServicesTableUpdate(String startTime, String completedTime, String serviceName, String comparitorFile) {
        File fileData = new File(comparitorFile);
        InputStream fileDataStream = null;
        try {
            fileDataStream = new FileInputStream(fileData);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int rowCountValue = getRowCountFromTable("Runs");
        Services services = new Services();
        services.setRunId(rowCountValue);
        services.setStartTime(startTime);
        services.setCompleteTime(completedTime);
        services.setServiceName(serviceName);
        LobHandler lobHandler = new DefaultLobHandler();
        services.setResult(new SqlLobValue(fileDataStream, (int)fileData.length(), lobHandler));
        int listOfDrops = DBUtilities.getDB("myntra").getJdbcTemplate()
                .update("INSERT INTO Services(runId, ServiceName, Result, StartedOn, CompletedOn) VALUES (?, ?, ?, ?, ?)", new Object[]{services.getRunId(), services.getServiceName(), services.getResult()
                        , services.getStartTime(), services.getCompleteTime()}, new int[] {Types.INTEGER, Types.VARCHAR, Types.BLOB, Types.TIMESTAMP, Types.TIMESTAMP});
        System.out.println(listOfDrops);

        /*int listOfDrops = DBUtilities.getDB("myntra_coupon").getJdbcTemplate()
                .update("INSERT INTO Services(runId, ServiceName, Result, StartedOn, CompletedOn) VALUES (?, ?, ?, ?, ?)", new Object[]{services.getRunId(), services.getServiceName(), services.getResult(), services.getStartTime(), services.getCompleteTime()});

        if (listOfDrops == 0) {
            LOG.error("\n\nUpdate failed for the db:- Runs. Query\n");
            LOG.error("Doing a retry");
            listOfDrops = DBUtilities.getDB("myntra_coupon").getJdbcTemplate()
                    .update("INSERT INTO Services(runId, ServiceName, Result, StartedOn, CompletedOn) VALUES (?, ?, ?, ?, ?)", new Object[]{services.getRunId(), services.getServiceName(), services.getResult(), services.getStartTime(), services.getCompleteTime()});
        }*/
    }


    public static int getRowCountFromTable(String tableName) {
        List<Map<String, Object>> itemCount = DBUtilities.getDB("myntra").getJdbcTemplate()
                .queryForList("SELECT count(*) FROM "+tableName);
        System.out.println(itemCount.get(0).get("count(*)"));
        Long rowData = (Long) itemCount.get(0).get("count(*)");
        return rowData.intValue();
    }


    public static void updateColumnForTable(String tableName, Map<String, String>columnNameValueMap) {
        int rowCountValue = getRowCountFromTable("Runs");
        for (Map.Entry mapElement : columnNameValueMap.entrySet()) {
            String key = (String) mapElement.getKey();
            String value = (String) mapElement.getValue();
            int isUpdtaed = DBUtilities.getDB("myntra").getJdbcTemplate()
                    .update("UPDATE " + tableName + " set " + key + " = ? where id ="+rowCountValue, new Object[]{value});
        }
    }


    /**
     * Get the current date time value
     * @return Date time value
     */
    public static String getCurrentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        return dateFormat.format(date);
    }


    /**
     * Get all the files in the specified directory
     * @param directoryPath The path where files are placed
     * @return List containing name of the files
     */
    public static List<String> getFileNameList(String directoryPath) {
        List<String> results = new ArrayList<String>();

        File[] files = new File(directoryPath).listFiles();

        for (File file : files) {
            if (file.isFile()) {
                results.add(file.getName());
            }
        }
        return results;

    }


    public static void writeToFile(List<Object> rowList, String fileName, boolean toAppend) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(fileName, toAppend);
            for(Object str: rowList) {
                writer.write(str + System.lineSeparator());
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static InputStream readFile(String fileName) {
        //String expectedFile = System.getProperty("user.dir") + "/src/main/resources/delta7mdb/healthCheck/ComparitorOutput/myntra_coupon/";
        File fileData = new File(fileName);
        InputStream fileDataStream = null;
        try {
            fileDataStream = new FileInputStream(fileData);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return fileDataStream;
    }
}
