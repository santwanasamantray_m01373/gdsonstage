package com.myntra.gdsStage;


import com.myntra.utils.config.ConfigManager;
import com.myntra.utils.db.DBUtilities;
import com.myntra.utils.logger.ILogger;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/* Created by Santwana.Samantray on 09/12/20 */

public class Service implements ILogger {

    protected static ConfigManager config = ConfigManager.getInstance();

    /**
     * Handles the operation required to update to gds. This first gets the .sql files which need to be updated
     * from the user and runs all the update commands inside those files.
     * It also does a one time retry, if the update query command failed.
     *
     * @param directoryPath The path where .sql files are present
     * @param filesToUpdate The comma seperated file names to be updated or, if the value passed is "", then it will execute
     *                      all commands in all the files prsent in above directoryPath
     * @return
     * @throws IOException
     */
    public static List<String> execute(String directoryPath, String filesToUpdate) throws IOException {

        List<String> fileNameList = null;
        if (filesToUpdate.equals("")){
            fileNameList = GdsUtils.getFileNameList(directoryPath);
        }
        else{
            String[] filesToUpdateArray = filesToUpdate.split(",");
            fileNameList = Arrays.asList(filesToUpdateArray);
        }

        try {
            //SSHTunnel.initializeTunneling();
            DBUtilities.initializeDBUtils();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String startTime = GdsUtils.getCurrentDateTime();

        for (String fileName : fileNameList) {

            String filePath = directoryPath + fileName;

            FileReader fr = new FileReader(filePath);

            List<String> queryList = GdsUtils.getStrings(fr);
            String dbName = fileName.split(".sql")[0];
            int listOfDrops = -1;
            for (String query : queryList) {
                try {
                    listOfDrops = DBUtilities.getDB("Hi").getJdbcTemplate()
                            .update(query);
                }  catch (Exception e) {
                    LOG.error("Not able to connect to the db "+dbName+ "Got the following error"+e.getMessage());
                }

                if(listOfDrops == 0) {
                    LOG.error("\n\nUpdate failed for the db:- "+dbName+
                            " . Query\n"+query);
                    LOG.error("Doing a retry");
                    listOfDrops = DBUtilities.getDB(dbName).getJdbcTemplate()
                            .update(query);
                    if(listOfDrops == 0) {
                        LOG.error("\n\nUpdate again failed for the db:- "+dbName+
                                " . Query\n"+query);
                    }
                    else if(listOfDrops == 1) {
                        LOG.info("Success fully updated for db :" +dbName+
                                " . Query\n"+query);
                    }
                }
                System.out.println("Rows affected = " + listOfDrops);
            }

        }
        /*String completedTime = GdsUtils.getCurrentDateTime();
        GdsUtils.doRunsTableUpdate(startTime, completedTime, fileNameList.toString(), "User1");*/
        return null;
    }

    private static List<String> getStrings(FileReader fr) throws IOException {
        List<String> queries = new ArrayList<String>();
        String read;
        BufferedReader br = new BufferedReader(fr);
        while ((read = br.readLine()) != null) {
            String[] query = read.split(";");
            for (String part : query) {
                queries.add(part);
            }
            LOG.debug(queries);
        }
        return queries;
    }


  /*  public static List<String> readSQLFile(String serviceName) throws IOException {

        String service = new ServiceProperties.valueOf("").getService();
        FileReader fr = new FileReader(System.getProperty("user.dir") + service);
        return getStrings(fr);
    }*/


    public static ArrayList<String> readDBConfigs(String serviceName) throws ParserConfigurationException,
            SAXException,
            IOException,
            NumberFormatException {

        String environment = config.getString("environment");
        //String serviceName = config.getString("serviceName");

        ArrayList<String> databases = new ArrayList<String>();

        URL dbConfigXml = DBUtilities.class.getClassLoader().getResource("/db-configuration.xml");
        if (null == dbConfigXml) {
            dbConfigXml = DBUtilities.class.getClassLoader().getResource("db-configuration.xml");
        }

        if (dbConfigXml == null) {
            LOG.warn("file " + dbConfigXml + " " + (dbConfigXml == null ? " is null" : "does not exists"));
        }
        File dbConfigFile = new File(dbConfigXml.getFile());
        if (dbConfigFile != null && dbConfigFile.exists()) {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = (Document) dBuilder.parse(dbConfigFile);
            doc.getDocumentElement().normalize();
            NodeList envList = doc.getElementsByTagName("environment");
            for (int env = 0; env < envList.getLength(); ++env) {
                Node nNode = envList.item(env);
                Element envElement = (Element) nNode;
                if (envElement.getAttribute("name").equals(environment)) {
                    NodeList connectionsList = envElement.getElementsByTagName("connectiondetails");

                    for (int connection = 0; connection < connectionsList.getLength(); ++connection) {
                        Node conNode = connectionsList.item(connection);
                        if (conNode.getNodeType() == 1) {
                            Element connectionElement = (Element) conNode;
                            String servicename = connectionElement.getAttribute("servicename");
                            if (servicename.equals(serviceName)) {
                                LOG.debug("servicename " + servicename);
                                String dbname = connectionElement.getAttribute("dbname");
                                databases.add(dbname);
                            }
                        }
                    }
                }
            }

        }
        return databases;
    }

    public static List<String> getFileNameList(String directoryPath) {
        List<String> results = new ArrayList<String>();

        File[] files = new File(directoryPath).listFiles();
        //If this pathname does not denote a directory, then listFiles() returns null.

        for (File file : files) {
            if (file.isFile()) {
                results.add(file.getName());
            }
        }
        return results;

    }
}
