package com.myntra.healthCheck;
/* Created by Santwana.Samantray on 09/12/20 */

import com.myntra.gdsStage.GdsUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Comparator {

    public static void doComparison(String tableName, String dbName) throws IOException {

        String expectedFile = System.getProperty("user.dir") + "/src/main/resources/delta7mdb/healthCheck/ExpectedResult/" +tableName;
        String actualFile = System.getProperty("user.dir") + "/src/main/resources/delta7mdb/healthCheck/ActualResult/" +tableName;
        List<Object> comparisonOutputList = new ArrayList<Object>();
        File file1= new File(expectedFile);
        File file2=new File(actualFile);
        boolean areEqual = true;

        int lineNum = 1;


        BufferedReader reader1 = new BufferedReader(new FileReader(file1));

        BufferedReader reader2 = new BufferedReader(new FileReader(file2));
        String line1 = reader1.readLine();

        String line2 = reader2.readLine();
        while (line1 != null || line2 != null) {
            if (line1 == null || line2 == null) {
                areEqual = false;
                comparisonOutputList.add("Table : "+tableName+"--- Mistmatch in line "+lineNum+". Expected result is : "+line1);
                break;
            } else if (!line1.equalsIgnoreCase(line2)) {
                areEqual = false;
                comparisonOutputList.add("Table : "+tableName+"--- Mistmatch in line "+lineNum+". Expected result is : "+line1);
                break;
            } else {
                comparisonOutputList.add("Table : "+tableName+"--- Everything is alright!!!!");
            }

            line1 = reader1.readLine();
            line2 = reader2.readLine();
            lineNum++;
        }
        GdsUtils.writeToFile(comparisonOutputList, System.getProperty("user.dir") + "/src/main/resources/delta7mdb/healthCheck/ComparitorOutput/"+dbName, true);

    }
}
