package com.myntra.healthCheck.dao;

import lombok.Data;
import org.springframework.jdbc.core.support.SqlLobValue;

@Data
public class Services {
    private int id;
    private int runId;
    private String serviceName;
    private SqlLobValue result;
    private String startTime;
    private String completeTime;
}
