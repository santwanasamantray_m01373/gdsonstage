package com.myntra.healthCheck.dao;

import lombok.Data;

@Data
public class Runs {

    private int id;
    private String User;
    private String services;
    private String startTime;
    private String completeTime;

}
