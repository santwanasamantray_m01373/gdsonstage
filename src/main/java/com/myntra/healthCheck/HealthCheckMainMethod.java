package com.myntra.healthCheck;

import com.myntra.gdsStage.Service;
import com.myntra.utils.db.DBUtilities;
import com.myntra.utils.logger.ILogger;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HealthCheckMainMethod implements ILogger {
    //static final Logger logger = Logger.getLogger(Service.class);

    public static void main(String[] args) throws IOException {

        /*java.io.InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("my.properties");
        java.util.Properties properties = new Properties();
        properties.load(inputStream);
        String appPort = properties.getProperty("app.port");
        String appDomain = properties.getProperty("app.domain");

        System.out.println(appPort);
        System.out.println(appDomain);*/
        //LOG.info("Hallo");/Users/300063666/git_New/myntra_core_automation_frmwk/src/main/resources/delta7mdb/healthCheck/selectQueries/myntra_coupon.sql
        String directoryPath = System.getProperty("user.dir") + "/src/main/resources/delta7mdb/healthCheck/selectQueries/";
        String fileNames = "myntra_coupon.sql, abc.sql";
        fileNames = "";

        /*String mydata = "select * from coupon_group_seller where id IN (1,2,3,4,5,6,7,8,9);";
        Pattern pattern = Pattern.compile("from(.*)where");
        Matcher matcher = pattern.matcher(mydata);
        if (matcher.find())
        {
            System.out.println(matcher.group(1));
        }*/

        try {
            HealthCheckService.execute(directoryPath, fileNames);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
