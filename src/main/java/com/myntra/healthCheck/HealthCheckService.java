package com.myntra.healthCheck;


import com.myntra.gdsStage.GdsUtils;
import com.myntra.utils.config.ConfigManager;
import com.myntra.utils.db.DBUtilities;
import com.myntra.utils.logger.ILogger;
import org.apache.commons.io.FileUtils;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.URL;
import java.sql.Types;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* Created by Santwana.Samantray on 09/12/20 */

public class HealthCheckService implements ILogger {

    protected static ConfigManager config = ConfigManager.getInstance();

    /**
     * Handles the operation required to update to gds. This first gets the .sql files which need to be updated
     * from the user and runs all the update commands inside those files.
     * It also does a one time retry, if the update query command failed.
     *
     * @param directoryPath The path where .sql files are present
     * @param filesToUpdate The comma seperated file names to be updated or, if the value passed is "", then it will execute
     *                      all commands in all the files prsent in above directoryPath
     * @return
     * @throws IOException
     */
    public static List<String> execute(String directoryPath, String filesToUpdate) throws IOException {

        FileUtils.cleanDirectory(new File(System.getProperty("user.dir") + "/src/main/resources/delta7mdb/healthCheck/ActualResult/"));
        FileUtils.cleanDirectory(new File(System.getProperty("user.dir") + "/src/main/resources/delta7mdb/healthCheck/ComparitorOutput/"));

        List<String> fileNameList = null;
        if (filesToUpdate.equals("")){
            fileNameList = GdsUtils.getFileNameList(directoryPath);
        }
        else{
            String[] filesToUpdateArray = filesToUpdate.split(",");
            fileNameList = Arrays.asList(filesToUpdateArray);
        }

        try {
            //SSHTunnel.initializeTunneling();
            DBUtilities.initializeDBUtils();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //writeToBlob();

        // Insert data to Runs table( without completed time. This data we update at the end of the run.)
        String startTime = GdsUtils.getCurrentDateTime();
        GdsUtils.doRunsTableUpdate(startTime, fileNameList.toString(), "User1");

        for (String fileName : fileNameList) {

            String filePath = directoryPath + fileName;

            FileReader fr = new FileReader(filePath);

            // Get all the select queries present for a fileName
            List<String> queryList = GdsUtils.getStrings(fr);
            String dbName = fileName.split(".sql")[0];

            // Get the start time(time just before comparison for services table)
            String startTimeServices = GdsUtils.getCurrentDateTime();

            for (String query : queryList) {
                try {
                    // Get the table name to be upadted from the query
                    String tableName = getTableName(query);

                    // Store the select query output into map(key -> column name, value -> column value)
                    List<Map<String, Object>> selectQueryOuputData = DBUtilities.getDB(dbName).getJdbcTemplate()
                            .queryForList(query);


                    // Get all the column values from above Map for each row and add to comma seprated string.
                    // Store each row containing the comma seperated values into a list
                    List<Object> rowList = getRowDataList(selectQueryOuputData);
                    System.out.println(selectQueryOuputData);

                    // Write the list to file, with each row written to a new line
                    GdsUtils.writeToFile(rowList, System.getProperty("user.dir") + "/src/main/resources/delta7mdb/healthCheck/ActualResult/" +tableName, false);

                    // Do the file comparison
                    Comparator.doComparison(tableName, dbName);
                    Thread.sleep(2000); // to be removed

                }  catch (Exception e) {
                    LOG.error("Not able to connect to the db "+dbName+ "Got the following error"+e.getMessage());
                }

            }
            // Get the end time
            String completedTimeServices = GdsUtils.getCurrentDateTime();


            // Update the services table for the particular services(table which was updated),
            GdsUtils.doServicesTableUpdate(startTimeServices, completedTimeServices, dbName, System.getProperty("user.dir") + "/src/main/resources/delta7mdb/healthCheck/ComparitorOutput/"+dbName);
            System.out.println("Service table updated for db" +dbName);

        }

        // get the completed time for Runs table
        String completedTime = GdsUtils.getCurrentDateTime();

        //Update the runs table with above time value
        Map<String, String> runsCompletedTimeMap = new HashMap<String, String>();
        runsCompletedTimeMap.put("CompletedOn", completedTime);
        GdsUtils.updateColumnForTable("Runs", runsCompletedTimeMap);
        System.out.println("Runs table updated");

        return null;
    }

    public static List<Object> getRowDataList(List<Map<String, Object>> rowDataInput) {
        List<Object> rowDataList = new ArrayList<Object>();
        for (int i = 0; i<rowDataInput.size(); i++) {
            String rows = "";
            for (Map.Entry<String, Object> pair : rowDataInput.get(i).entrySet()) {

//            i += pair.getKey() + pair.getValue();
                System.out.println(pair.getValue());
                rows = rows + pair.getValue() + ",";
            }
            String b = rows.substring(0, rows.length()-1);
            rowDataList.add(b);
        }
        System.out.println(rowDataList);
        return rowDataList;

    }

    private static List<String> getStrings(FileReader fr) throws IOException {
        List<String> queries = new ArrayList<String>();
        String read;
        BufferedReader br = new BufferedReader(fr);
        while ((read = br.readLine()) != null) {
            String[] query = read.split(";");
            for (String part : query) {
                queries.add(part);
            }
            LOG.debug(queries);
        }
        return queries;
    }


  /*  public static List<String> readSQLFile(String serviceName) throws IOException {

        String service = new ServiceProperties.valueOf("").getService();
        FileReader fr = new FileReader(System.getProperty("user.dir") + service);
        return getStrings(fr);
    }*/


    public static ArrayList<String> readDBConfigs(String serviceName) throws ParserConfigurationException,
            SAXException,
            IOException,
            NumberFormatException {

        String environment = config.getString("environment");
        //String serviceName = config.getString("serviceName");

        ArrayList<String> databases = new ArrayList<String>();

        URL dbConfigXml = DBUtilities.class.getClassLoader().getResource("/db-configuration.xml");
        if (null == dbConfigXml) {
            dbConfigXml = DBUtilities.class.getClassLoader().getResource("db-configuration.xml");
        }

        if (dbConfigXml == null) {
            LOG.warn("file " + dbConfigXml + " " + (dbConfigXml == null ? " is null" : "does not exists"));
        }
        File dbConfigFile = new File(dbConfigXml.getFile());
        if (dbConfigFile != null && dbConfigFile.exists()) {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = (Document) dBuilder.parse(dbConfigFile);
            doc.getDocumentElement().normalize();
            NodeList envList = doc.getElementsByTagName("environment");
            for (int env = 0; env < envList.getLength(); ++env) {
                Node nNode = envList.item(env);
                Element envElement = (Element) nNode;
                if (envElement.getAttribute("name").equals(environment)) {
                    NodeList connectionsList = envElement.getElementsByTagName("connectiondetails");

                    for (int connection = 0; connection < connectionsList.getLength(); ++connection) {
                        Node conNode = connectionsList.item(connection);
                        if (conNode.getNodeType() == 1) {
                            Element connectionElement = (Element) conNode;
                            String servicename = connectionElement.getAttribute("servicename");
                            if (servicename.equals(serviceName)) {
                                LOG.debug("servicename " + servicename);
                                String dbname = connectionElement.getAttribute("dbname");
                                databases.add(dbname);
                            }
                        }
                    }
                }
            }

        }
        return databases;
    }

    public static List<String> getFileNameList(String directoryPath) {
        List<String> results = new ArrayList<String>();

        File[] files = new File(directoryPath).listFiles();
        //If this pathname does not denote a directory, then listFiles() returns null.

        for (File file : files) {
            if (file.isFile()) {
                results.add(file.getName());
            }
        }
        return results;

    }

    /*public static void writeToFile(List<Object> rowList, String fileName) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(fileName);
            for(Object str: rowList) {
                writer.write(str + System.lineSeparator());
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    public static String getTableName(String query) {
        //String mydata = "select * from coupon_group_seller where id IN (1,2,3,4,5,6,7,8,9);";
        Pattern pattern = Pattern.compile("from(.*)where");
        Matcher matcher = pattern.matcher(query);
        if (matcher.find())
        {
            System.out.println(matcher.group(1));
            return matcher.group(1).trim();
        }
        return "";
    }

    public static void writeToBlob() {
        String expectedFile = System.getProperty("user.dir") + "/src/main/resources/delta7mdb/healthCheck/ComparitorOutput/myntra_coupon/";
        File image = new File(expectedFile);
        InputStream imageIs = null;
        try {
            imageIs = new FileInputStream(image);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        LobHandler lobHandler = new DefaultLobHandler();
        int listOfDrops = DBUtilities.getDB("myntra_coupon").getJdbcTemplate()
                .update("INSERT INTO Services(runId, ServiceName, Result, StartedOn, CompletedOn) VALUES (?, ?, ?, ?, ?)", new Object[]{6, "hiii", new SqlLobValue(imageIs, (int)image.length(), lobHandler)
                        , "2021-01-15 11:32:37", "2021-01-15 11:32:37"}, new int[] {Types.INTEGER, Types.VARCHAR, Types.BLOB, Types.TIMESTAMP, Types.TIMESTAMP});
        System.out.println(listOfDrops);
    }


}
