package com.myntra.utils.redis;
import com.myntra.utils.redis.RedisUtil;
import com.myntra.utils.logger.ILogger;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.jedis.exceptions.JedisMovedDataException;

import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
/* Created by Santwana.Samantray on 04/12/20 */

public class RedisConnector implements ILogger {
    private static RedisConnector lgpHelper;


    public synchronized static RedisConnector getInstance() {
        if (lgpHelper == null) {
            lgpHelper = new RedisConnector();
        }
        return lgpHelper;
    }


    /**
     * Get all the keys available in redis cluster
     *
     * @return {@link String}
     *  @throws Exception
     */
    public String getAllKeys() throws Exception {
        List<Jedis> jedisConnections = RedisUtil.getInstance().getConnection();
        String keys = null;
        for (Jedis jedis : jedisConnections) {
            try {
                ScanParams scanParams = new ScanParams().count(10000).match("*");
                String cur = "0";
                boolean cycleIsFinished = false;
                while(!cycleIsFinished) {

                    ScanResult<String> scanResult = jedis.scan(cur, scanParams);
                    List<String> result = scanResult.getResult();
                    LOG.info("Keys " + result);
                    cur = scanResult.getStringCursor();

                    if (cur.equals("0")) {
                        cycleIsFinished = true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                jedis.close();
            }
        }
        return keys;
    }



    /**
     *  If key exists then fetch the value for a specified key of type string
     *
     * @param key      {@link String}
     * @return {@link String}
     * @throws JedisMovedDataException
     */
    public String getStringValue(String key) throws Exception {
        List<Jedis> jedisConnections = RedisUtil.getInstance().getConnection();
        String value = null;
        for (Jedis jedis : jedisConnections) {
            try {
                LOG.debug("The server is running :" + jedis.ping());
                if (jedis.exists(key)) {
                    value = jedis.get(key);
                    LOG.info("Getting a value from redis : " + value);
                }break;
            }catch (JedisMovedDataException e) {
                e.printStackTrace();
            }
        }
        return value;
    }


    /**
     *  If key exists then fetch the value for a specified key of type Set
     *
     * @param key      {@link String}
     * @return {@link String}
     * @throws JedisMovedDataException
     */
    public List<String> getSetValue(String key) throws Exception {
        List<Jedis> jedisConnections = RedisUtil.getInstance().getConnection();
        List<String> result = null;
        for (Jedis jedis : jedisConnections) {
            try {
                ScanParams scanParams = new ScanParams().count(10000);
                String cur = "0";
                boolean cycleIsFinished = false;
                while(!cycleIsFinished) {
                    ScanResult<String> scanResult = jedis.sscan(key, cur, scanParams);
                    result = scanResult.getResult();
                    LOG.info("key - " + key + " : value - " + result);
                    cur = scanResult.getStringCursor();

                    if (cur.equals("0")) {
                        cycleIsFinished = true;
                    }
                }break;
            }catch (JedisMovedDataException e) {
                e.printStackTrace();
            }
        }
        return result;
    }


    /**
     *  If key exists then fetch the value for a specified key of type HashMap
     *
     * @param key      {@link String}
     * @return {@link String}
     * @throws JedisMovedDataException
     */
    public List<Entry<String, String>> getHashValue(String key) throws Exception {
        List<Jedis> jedisConnections = RedisUtil.getInstance().getConnection();
        List<Entry<String, String>> result = null;
        for (Jedis jedis : jedisConnections) {
            try {
                ScanParams scanParams = new ScanParams().count(10000);
                String cur = "0";
                boolean cycleIsFinished = false;
                while(!cycleIsFinished) {
                    ScanResult<Entry<String, String>> scanResult = jedis.hscan(key, cur, scanParams);
                    result = scanResult.getResult();
                    LOG.info("key - " + key + " : value - " + result);

                    cur = scanResult.getStringCursor();
                    if (cur.equals("0")) {
                        cycleIsFinished = true;
                    }
                }break;
            }catch (JedisMovedDataException e) {
                e.printStackTrace();
            }
        }
        return result;
    }


    /**
     * Get all keys and values in a redis cluster
     *
     * @return {@link String}
     * @throws Exception
     */
    public String getAllKeysAndValues() throws Exception {
        List<Jedis> jedisConnections = RedisUtil.getInstance().getConnection();
        String keys = null;
        for (Jedis jedis : jedisConnections) {
            try {
                LOG.debug("The server is running " + jedis.ping());
                Set<String> names=jedis.keys("*");


                Iterator<String> it = names.iterator();
                while (it.hasNext()) {
                    keys = it.next();
                    String keyType = jedis.type(keys);
                    switch (keyType) {

                        case "string":
                            LOG.info(keys + " : " + jedis.get(keys));
                            break;

                        case "hash":
                            LOG.info(keys + " : " + jedis.hgetAll(keys));
                            break;

                        case "set":
                            LOG.info(keys + " : " + jedis.smembers(keys));
                            break;

                        case "list":
                            LOG.info(keys + " : " + jedis.lrange(keys, 0, -1));
                            break;

                        default:
                            break;
                    }

                }break;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return keys;
    }


    /**
     * Inserting a value for a existing key.
     * if value is null for a key then new value will be inserted if value is not null then value will be overridden.
     *
     * @param key      {@link String}
     * @param value      {@link String}
     * @throws JedisMovedDataException
     */
    public synchronized void updateValue(String key, String value) throws Exception {
        List<Jedis> jedisConnections = RedisUtil.getInstance().getConnection();
        for (Jedis jedis : jedisConnections) {
            try {
                LOG.debug("The server is running " + jedis.ping());
                if (jedis.get(key) != null) {
                    jedis.set(key, value);
                }break;
            } catch (JedisMovedDataException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     *  Inserting the key and value.
     *
     * @param key      {@link String}
     * @param value      {@link String}
     * @throws JedisMovedDataException
     */
    public synchronized void setKeyValue(String key, String value) throws Exception {
        List<Jedis> jedisConnections = RedisUtil.getInstance().getConnection();
        for (Jedis jedis : jedisConnections) {
            try {
                LOG.debug("The server is running " + jedis.ping());
                jedis.set(key, value);
                break;
            } catch (JedisMovedDataException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Delete the key, if it exists.
     *
     * @param key      {@link String}
     * @throws JedisMovedDataException
     */
    public void deleteKey(String key) throws Exception {
        List<Jedis> jedisConnections = RedisUtil.getInstance().getConnection();
        for (Jedis jedis : jedisConnections) {
            try {
                LOG.debug("The server is running " + jedis.ping());
                if (jedis.get(key) != null) {
                    jedis.del(key);
                }break;
            } catch (JedisMovedDataException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Deleting all keys in a cluster
     *
     * @throws JedisMovedDataException
     */
    public synchronized void flushAll() throws Exception {
        List<Jedis> jedisConnections = RedisUtil.getInstance().getConnection();
        for (Jedis jedis : jedisConnections) {
            try {
                LOG.debug("The server is running " + jedis.ping());
                if (jedis.keys("*") != null) {
                    jedis.flushAll();
                }break;
            } catch (JedisConnectionException e) {
                e.printStackTrace();
            }
        }
    }



    /**
     * Get the matching keys using pattern search
     *
     *
     *	@param pattern      {@link String}
     * 	@return {@link String}
     *  @throws Exception
     */
    public String getMatchingKeys(String pattern) throws Exception {
        List<Jedis> jedisConnections = RedisUtil.getInstance().getConnection();
        String keys = null;
        for (Jedis jedis : jedisConnections) {
            try {
                ScanParams scanParams = new ScanParams().count(10000).match(pattern);
                String cur = "0";
                boolean cycleIsFinished = false;
                while(!cycleIsFinished) {

                    ScanResult<String> scanResult = jedis.scan(cur, scanParams);
                    List<String> result = scanResult.getResult();
                    if(!result.isEmpty())
                        LOG.info("Keys " + result);
                    else
                        LOG.info("No matching keys found");

                    cur = scanResult.getStringCursor();
                    if (cur.equals("0")) {
                        cycleIsFinished = true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                jedis.close();
            }
        }
        return keys;
    }


    /**
     * Get the keys using pattern search and delete the keys
     *
     * @param pattern      {@link String}
     * @param key      {@link String}
     * @throws JedisMovedDataException
     */
    public void deleteMatchingKeys(String pattern) throws Exception {

        List<Jedis> jedisConnections = RedisUtil.getInstance().getConnection();
        for (Jedis jedis : jedisConnections) {
            try {
                Set<String> names=jedis.keys(pattern);
                if(names.size() > 0) {
                    for(String keys : names) {
                        jedis.del(keys);
                        LOG.info("Deleting the key from redis cluser :" + keys);
                    }
                }else {
                    LOG.info("No matching keys found using the pattern search to delete : " + jedis);
                }
            } catch (JedisMovedDataException e) {
                e.printStackTrace();
            }
        }
    }

}