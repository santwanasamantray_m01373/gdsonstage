package com.myntra.utils.redis;
import com.myntra.utils.config.ConfigManager;
import com.myntra.utils.logger.ILogger;

import redis.clients.jedis.Jedis;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;



import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
/* Created by Santwana.Samantray on 04/12/20 */

public class RedisUtil implements ILogger {

    private static List < Jedis > jedisConnections;
    public static RedisUtil lgpRedisConnection;
    protected static ConfigManager config = ConfigManager.getInstance();

    public synchronized static RedisUtil getInstance() throws Exception {
        if (lgpRedisConnection == null) {
            lgpRedisConnection = new RedisUtil();

            if (jedisConnections == null) {
                try {
                    setRedisConnection();
                } catch (ParserConfigurationException e) {
                    e.printStackTrace();
                } catch (SAXException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return lgpRedisConnection;
    }

    public List < Jedis > getConnection() {

        LOG.info("Getting connections : " + jedisConnections);
        return jedisConnections;
    }




    /**
     * This method is used to initialize Redis Utils by taking all the environments
     * details from redis-configuration.xml and config.properties.
     *
     *
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @author gunesh.p@myntra.com
     */
    private static void setRedisConnection() throws ParserConfigurationException, SAXException, IOException {

        jedisConnections = new ArrayList < Jedis > ();
        URL redisConfigXml = RedisUtil.class.getClassLoader().getResource("/redis-configuration.xml");
        if (null == redisConfigXml) {
            redisConfigXml = RedisUtil.class.getClassLoader().getResource("redis-configuration.xml");
        }

        if (redisConfigXml == null) {
            LOG.error("redis-configuration.xml not found");
            return;
        }

        LOG.debug("redisConfigXml :: " + redisConfigXml);
        LOG.debug("Redis configuration file loaded successfully.");

        String environment = config.getString("environment");
        File redisConfigFile = new File(redisConfigXml.getFile());
        if (redisConfigFile != null && redisConfigFile.exists()) {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = (Document) dBuilder.parse(redisConfigFile);
            doc.getDocumentElement().normalize();
            NodeList envList = doc.getElementsByTagName("environment");

            for (int i = 0; i < envList.getLength(); ++i) {
                Node nNode = envList.item(i);
                Element envElement = (Element) nNode;
                if (envElement.getAttribute("name").equals(environment)) {
                    NodeList connectionsList = envElement.getElementsByTagName("connectiondetails");

                    for (int j = 0; j < connectionsList.getLength(); ++j) {
                        Node conNode = connectionsList.item(j);
                        if (conNode.getNodeType() == 1) {
                            Element connectionElement = (Element) conNode;
                            String host = connectionElement.getAttribute("host");
                            LOG.info(host);
                            String ports[] = connectionElement.getAttribute("localPort").split(",");
                            for (String port: ports) {
                                LOG.info(Integer.parseInt(port));
                            }
                            for (String port: ports) {
                                //jedisConnections.add(new Jedis(new HostAndPort(host, Integer.parseInt(port))));
                                jedisConnections.add(new Jedis(host, Integer.parseInt(port), 600000, 600000));
                            }
                            LOG.info("Redis connection is successfull to all the ports.");

                        }
                    }
                } else {

                    LOG.info("No such environment details found in redis-configuration.xml");
                }
            }
        }


    }


    public static void closeRedisConnections() {
        for (int i = 0; i < jedisConnections.size(); i++) {
            if (jedisConnections.get(i).isConnected()) {
                jedisConnections.get(i).close();
            }
        }
    }


}