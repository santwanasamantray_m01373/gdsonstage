package com.myntra.utils.db;/* Created by Santwana.Samantray on 04/12/20 */

import com.myntra.utils.logger.ILogger;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

public interface DatabaseDAO extends ILogger {

    /**
     * This Method is used to set the dataSource Object for the JDBC Template
     *
     * @param dataSource
     * @author manoj.behera@myntra.com
     */
    public void setDataSource(DataSource dataSource);

    /**
     * This Method will fetch Single record and return it in a Map of Key and Value
     * pair.
     *
     * @param sqlQuery - will provide the sql query which will return a single
     *                 record.
     * @return the results in a Map of Key and Value pair
     * @author manoj.behera@myntra.com
     */
    public Map<String, Object> fetchRecord(String sqlQuery);

    /**
     * This Method will fetch multiple records and return it in a list of Map and
     * Map will contain Key and Value pair.
     *
     * @param sqlQuery - will provide the sql query which will return a single or
     *                 multiple records.
     * @return A list of Map and Map will contain Key and Value pair.
     * @author manoj.behera@myntra.com
     */
    public List<Map<String, Object>> fetchRecords(String sqlQuery);

    /**
     * This Method will update record and return a integer value of 0 or 1, 1 -
     * Updated Successfully and 0 - Not updated.
     *
     * @param sqlQuery - will provide the sql query which will return a single or
     *                 multiple records.
     * @return A integer value of 0 or 1, 1 - Updated Successfully and 0 - Not
     *         updated.
     * @author manoj.behera@myntra.com
     */
    public int updateRecord(String sqlQuery);

    /**
     * Get instance of the created {@link JdbcTemplate}
     *
     * @return {@link JdbcTemplate}
     */
    public JdbcTemplate getJdbcTemplate();

}