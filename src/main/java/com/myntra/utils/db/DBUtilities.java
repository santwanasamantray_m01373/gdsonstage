package com.myntra.utils.db;
import com.myntra.utils.config.ConfigManager;
import com.myntra.utils.config.ConfigProperties;
import com.myntra.utils.impl.AbstractDAO;
import com.myntra.utils.impl.CassandraConnector;
import com.myntra.utils.impl.Credentials;
import com.myntra.utils.logger.ILogger;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
/* Created by Santwana.Samantray on 04/12/20 */

public class DBUtilities implements ILogger {
    public static List < HikariDataSource > connections = new ArrayList < > ();
    protected static ConfigManager config;
    protected static ApplicationContext context;


    static {
        config = ConfigManager.getInstance();
        context = new ClassPathXmlApplicationContext("classpath*:/db-config.xml");

    }

    /**
     * This method is used to load all db properties according to the environments
     *
     * @param dbSets<DBConfigEntity> dbSets
     * @return The List<HikariDataSource> Objects.
     * @author gunesh.p@myntra.com
     */
    private static List < HikariDataSource > loadAllDBs(Set < AbstractDAO > dbSets) {
        for (Iterator < AbstractDAO > iterator = dbSets.iterator(); iterator.hasNext();) {
            AbstractDAO dataBaseDAO = iterator.next();

            HikariDataSource dataSource = new HikariDataSource();
            dataSource.setDataSourceClassName(dataBaseDAO.getDriverClassName());
            dataSource.setUsername(dataBaseDAO.getUsername());
            dataSource.setPassword(dataBaseDAO.getPassword());
            dataSource.setIdleTimeout(10000);
            dataSource.setMaximumPoolSize(config.getInt(ConfigProperties.DB_MAX_POOL_SIZE.getKey()));
            dataSource.setConnectionTimeout(30000);
            dataSource.setValidationTimeout(5000);
            dataSource.setMaxLifetime(1800000);
            Properties properties = new Properties();

            properties.setProperty("serverName", getHostName(dataBaseDAO.getHostname()));
            properties.setProperty("port", dataBaseDAO.getPortname());
            properties.setProperty("databaseName", dataBaseDAO.getDbName());
            dataSource.setDataSourceProperties(properties);
            DatabaseDAO jt = null;
            if (dataBaseDAO.getDriverClassName().contains("mysql"))
                jt = new MySqlConnector(dataBaseDAO.getDbName(), dataBaseDAO.getDriverClassName(),
                        getHostName(dataBaseDAO.getHostname()), dataBaseDAO.getPortname(), dataBaseDAO.getUsername(),
                        dataBaseDAO.getPassword());
            if (dataBaseDAO.getDriverClassName().contains("cassandra"))
                jt = new CassandraConnector(dataBaseDAO.getDbName(), dataBaseDAO.getDriverClassName(),
                        getHostName(dataBaseDAO.getHostname()), dataBaseDAO.getPortname(), dataBaseDAO.getUsername(),
                        dataBaseDAO.getPassword());
            jt.setDataSource(dataSource);
            connections.add(dataSource);
            if (!((ConfigurableApplicationContext) context).getBeanFactory().containsBean(dataBaseDAO.getDbName())) {
                ((ConfigurableApplicationContext) context).getBeanFactory().registerSingleton(dataBaseDAO.getDbName(),
                        jt);
            }

        }
        LOG.debug("All DB Configurations are loaded successfully.");
        return connections;
    }

    private static String getHostName(String hostname) {
        return ConfigManager.getInstance().getBoolean(ConfigProperties.IS_CI_CD.getKey()) ?
                ConfigManager.getInstance().getString(ConfigProperties.CI_CD_DB_HOST.getKey()) :
                hostname;
    }

    /**
     * This method is used to read the db-configuration property and set to
     * DBConfigEntity. Note : Current used DocumentBuilderFactory to read the xml
     * properties, later on will move to JAXB formatted. That will reduce the
     * current code.
     *
     * @param environment
     * @param dbConfigFile
     * @return The Set<DBConfigEntity> Objects.
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException                  , {@link Exception}
     * @author gunesh.p@myntra.com
     */
    private static Set < AbstractDAO > readDBConfigurations(String environment, File dbConfigFile)
            throws ParserConfigurationException, SAXException, IOException {
        Set < AbstractDAO > dbSets = new HashSet < > ();
        if (dbConfigFile == null || !dbConfigFile.exists()) {
            LOG.warn("file " + dbConfigFile + " " + (dbConfigFile == null ? " is null" : "does not exists"));
            return dbSets;
        }
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(dbConfigFile);
        doc.getDocumentElement().normalize();
        NodeList envList = doc.getElementsByTagName("environment");

        for (int i = 0; i < envList.getLength(); i++) {
            Node nNode = envList.item(i);
            Element envElement = (Element) nNode;
            if (envElement.getAttribute("name").equals(environment)) {
                NodeList connectionsList = envElement.getElementsByTagName("connectiondetails");

                for (int j = 0; j < connectionsList.getLength(); j++) {

                    Node conNode = connectionsList.item(j);

                    if (conNode.getNodeType() == Node.ELEMENT_NODE) {

                        Element connectionElement = (Element) conNode;
                        AbstractDAO conn = null;
                        String connType = connectionElement.getAttribute("connectionmethod");
                        switch (connType) {
                            case "jdbc":
                                /**
                                 * This code will check for the credentialtype in db-configuration.xml and based
                                 * on that will proceed for the static or vault credentials.If we are not
                                 * providing the credentialtype also it will take as default as static.
                                 *
                                 * @author gunesh.p@myntra.com
                                 */

                                Credentials credentials = null;

                                credentials = new Credentials(connectionElement.getAttribute("username"),
                                        connectionElement.getAttribute("password"));
                                LOG.debug("Static Username : " + credentials.getUserName() + " :: " +
                                        "Static Password : " + credentials.getPassword());

                                conn = new MySqlConnector(connectionElement.getAttribute("dbname"),
                                        "com.mysql.cj.jdbc.MysqlDataSource",
                                        connectionElement.getAttribute("hostname"), connectionElement.getAttribute("port"),
                                        credentials.getUserName(), credentials.getPassword());
                                dbSets.add(conn);
                                break;
                            case "cassandra":
                                conn = new CassandraConnector(connectionElement.getAttribute("dbname"),
                                        "org.apache.cassandra.cql.jdbc.CassandraDriver",
                                        connectionElement.getAttribute("hostname"), connectionElement.getAttribute("port"),
                                        connectionElement.getAttribute("username"),
                                        connectionElement.getAttribute("password"));
                                dbSets.add(conn);
                                break;
                            default:
                                conn = new MySqlConnector(connectionElement.getAttribute("dbname"),
                                        "com.mysql.cj.jdbc.MysqlDataSource",
                                        connectionElement.getAttribute("hostname"), connectionElement.getAttribute("port"),
                                        connectionElement.getAttribute("username"),
                                        connectionElement.getAttribute("password"));
                                dbSets.add(conn);
                        }
                    }
                }
            }
        }
        return dbSets;
    }

    /**
     * This method is used to get the datasource object according to the
     * environment, so taht we can fetch data from the appropriate DB.
     *
     * @param dbName - The DB name should be unique and as per the configurations.
     * @return The DataBaseDAO Object.
     * @author gunesh.p@myntra.com
     */
    public static DatabaseDAO getDB(String dbName) {
        return context.getBean(dbName, AbstractDAO.class);
    }

    /**
     * This method is used to initialize DB Utils by taking all the environments
     * details.
     *
     * @throws Exception
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @author gunesh.p@myntra.com
     */
    public static void initializeDBUtils() throws Exception, ParserConfigurationException, SAXException, IOException {
        //String environment = config.getString("environment");
        String environment = "stage";


        URL dbConfigXml = DBUtilities.class.getClassLoader().getResource("/db-configuration.xml");

        if (null == dbConfigXml) {
            dbConfigXml = DBUtilities.class.getClassLoader().getResource("db-configuration.xml");
        }

        if (dbConfigXml == null) {
            LOG.error("db-configuration.xml not found");
            return;
        }

        LOG.debug("dbConfigXml :: " + dbConfigXml);
        LOG.debug("DB configuration file loaded successfully.");

        File dbConfigFile = new File(dbConfigXml.getFile());

        Set < AbstractDAO > dbSets = readDBConfigurations(environment, dbConfigFile);
        LOG.debug("DB configuration details are read and set to the entity successfully.");

        connections = loadAllDBs(dbSets);
        LOG.info("All DB configurations are loadded successfully.");




    }

    /**
     * This Method will close all DB connections which are opened using the
     * connections.
     *
     * @author gunesh.p@myntra.com
     */
    public static void closeDBConnections() {
        for (int i = 0; i < connections.size(); i++) {
            if (!connections.get(i).isClosed()) {
                connections.get(i).close();
            }
        }
    }

}