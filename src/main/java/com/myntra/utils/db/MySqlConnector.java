package com.myntra.utils.db;

import com.myntra.utils.impl.AbstractDAO;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;
/* Created by Santwana.Samantray on 04/12/20 */
@Component
public class MySqlConnector extends AbstractDAO {

    public MySqlConnector(String dbName, String driverClassName, String hostname, String portname, String username,
                          String password) {
        super(dbName, driverClassName, hostname, portname, username, password);
    }

    private JdbcTemplate jdbcTemplate;

    @Override
    public void setDataSource(DataSource dataSource) {
        LOG.debug("DataSource :- " + dataSource);
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public Map<String, Object> fetchRecord(String sqlQuery) {
        LOG.debug("SqlQuery :- " + sqlQuery);
        try {
            return jdbcTemplate.queryForMap(sqlQuery);
        } catch (Exception ex) {
            LOG.info(ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Map<String, Object>> fetchRecords(String sqlQuery) {
        LOG.debug("SqlQuery :- " + sqlQuery);
        try {
            return this.jdbcTemplate.queryForList(sqlQuery);
        } catch (Exception ex) {
            LOG.info(ex.getMessage());
            return null;
        }
    }

    @Override
    public int updateRecord(String sqlQuery) {
        LOG.debug("SqlQuery :- " + sqlQuery);
        try {
            return this.jdbcTemplate.update(sqlQuery);
        } catch (Exception ex) {
            LOG.info(ex.getMessage());
            return 0;
        }
    }

    @Override
    public JdbcTemplate getJdbcTemplate() {
        return this.jdbcTemplate;
    }
}

