package com.myntra.utils.impl;
import com.datastax.driver.core.*;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* Created by Santwana.Samantray on 04/12/20 */

public class CassandraConnector extends AbstractDAO {

    public CassandraConnector(String dbName, String driverClassName, String hostname, String portname, String username,
                              String password) {
        super(dbName, driverClassName, hostname, portname, username, password);
    }

    private ResultSet result;

    private Cluster cluster;

    private Session session;

    @Override
    public void setDataSource(DataSource dataSource) {
        connect(hostname, Integer.parseInt(portname));
    }

    @Override
    public Map<String, Object> fetchRecord(String cqlQuery) {
        LOG.debug("cqlQuery :- " + cqlQuery);
        Map<String, Object> record = new HashMap<>();
        try {
            result = session.execute(cqlQuery);
            Row row = result.one();

            if (row != null) {
                ColumnDefinitions columnDefinitions = row.getColumnDefinitions();
                for (ColumnDefinitions.Definition columnDefinition : columnDefinitions) {
                    String name = columnDefinition.getName();
                    Object value = row.getObject(name);
                    record.put(name, value);
                }
            }
        } catch (Exception e) {
            LOG.info(e.getMessage());
        } finally {

        }
        return record;
    }

    @Override
    public List<Map<String, Object>> fetchRecords(String cqlQuery) {
        LOG.debug("cqlQuery :- " + cqlQuery);
        List<Map<String, Object>> records = new ArrayList<>();
        try {
            result = session.execute(cqlQuery);
            List<Row> rows = result.all();
            if (!rows.isEmpty()) {
                for (Row row : rows) {
                    Map<String, Object> record = new HashMap<>();
                    ColumnDefinitions columnDefinitions = row.getColumnDefinitions();
                    for (ColumnDefinitions.Definition columnDefinition : columnDefinitions) {
                        String name = columnDefinition.getName();
                        Object value = row.getObject(name);
                        record.put(name, value);
                    }
                    records.add(record);
                }
            }
        } catch (Exception e) {
            LOG.info(e.getMessage());
        } finally {

        }
        return records;
    }

    @Override
    public int updateRecord(String cqlQuery) {
        LOG.debug("cqlQuery :- " + cqlQuery);
        int flag = 0;
        try {
            result = session.execute(cqlQuery);
        } catch (Exception e) {
            LOG.info(e.getMessage());
            flag = 1;
        }
        return flag;
    }

    /**
     * This method implementation is not required here for Cassandra conn
     */
    @Override
    public JdbcTemplate getJdbcTemplate() {
        return null; // No Implementation is required
    }

    /**
     * This method is to provision a session to the Cassandra Cluster/DB
     *
     * @author gunesh.p
     */
    private void connect(final String node, final int port) {
        this.cluster = Cluster.builder().addContactPoint(node).withPort(port).build();
        final Metadata metadata = cluster.getMetadata();
        LOG.debug("Connected to cluster: %s\n " + metadata.getClusterName());
        for (final Host host : metadata.getAllHosts()) {
            LOG.debug("Datacenter: %s; Host: %s; Rack: %s\n " + host.getDatacenter() + host.getAddress()
                    + host.getRack());
        }
        session = cluster.connect();
    }

}
