package com.myntra.utils.impl;

/* Created by Santwana.Samantray on 04/12/20 */

public class Credentials {
    private String credentialId;
    private String credentialSecret;

    public Credentials(String credentialId, String credentialSecret) {
        this.credentialId = credentialId;
        this.credentialSecret = credentialSecret;
    }

    public String getUserName() {
        return credentialId;
    }

    public String getPassword() {
        return credentialSecret;
    }
}
