package com.myntra.utils.impl;

import com.myntra.utils.db.DatabaseDAO;
import com.myntra.utils.logger.ILogger;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Map;
/* Created by Santwana.Samantray on 04/12/20 */

public abstract class AbstractDAO implements ILogger, DatabaseDAO {

    protected String dbName;
    protected String driverClassName;
    protected String hostname;
    protected String portname;
    protected String username;
    protected String password;

    public AbstractDAO(String dbName, String driverClassName, String hostname, String portname, String username,
                       String password) {
        this.dbName = dbName;
        this.driverClassName = driverClassName;
        this.hostname = hostname;
        this.portname = portname;
        this.username = username;
        this.password = password;
    }

    public String getDbName() {
        return dbName;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public String getHostname() {
        return hostname;
    }

    public String getPortname() {
        return portname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public abstract Map<String, Object> fetchRecord(String sqlQuery);

    /**
     * This method implementation is not required here for Cassandra conn
     */
    public JdbcTemplate getJdbcTemplate() {
        // TODO Auto-generated method stub
        return null;
    }



}
