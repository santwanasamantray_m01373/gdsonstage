package com.myntra.utils.impl;


/* Created by Santwana.Samantray on 04/12/20 */

import java.io.IOException;

public interface CredentialProvider {
    Credentials getCredentials();

    void refresh() throws IOException;
}
