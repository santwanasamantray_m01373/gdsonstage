package com.myntra.utils.logger;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
/* Created by Santwana.Samantray on 04/12/20 */

public interface ILogger {

    public static final Logger LOG = LogManager.getLogger(ILogger.class);
}
