package com.myntra.utils.config;

import com.myntra.utils.logger.ILogger;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;

import java.net.URL;
/* Created by m01373 on 04/12/20 */


public class ConfigManager implements ILogger {

    private static ConfigManager configManager;
    PropertiesConfiguration defaultConfig = null;
    PropertiesConfiguration overrideConfig = null;
    CompositeConfiguration config = new CompositeConfiguration();

    private ConfigManager() {

        try {

            LOG.info(
                    "Configurations are loaded from config.properties and default.properties, where config.properties taking preference");

            URL configFile = ConfigManager.class.getClassLoader().getResource("/config.properties");

            if (configFile == null) {
                configFile = ConfigManager.class.getClassLoader().getResource("config.properties");
                if (configFile == null) {
                    //   throw new APIException("could not load config.properties");
                }
            }

            overrideConfig = new PropertiesConfiguration(configFile);

            URL propFileURL = ConfigManager.class.getClassLoader().getResource("/default.properties");

            if (propFileURL == null) {
                propFileURL = ConfigManager.class.getClassLoader().getResource("default.properties");
                if (propFileURL == null) {
                    //    throw new APIException("could not load default.properties");
                }
            }

            defaultConfig = new PropertiesConfiguration(propFileURL);

            overrideConfig.setAutoSave(true);
            defaultConfig.setAutoSave(true);

            config.addConfiguration(overrideConfig);
            config.addConfiguration(defaultConfig);

        } catch (ConfigurationException e) {

            e.printStackTrace();
        }

    }

    /**
     * Get a singleton instance of {@link ConfigManager}
     *
     * @return {@link ConfigManager}
     */
    public static ConfigManager getInstance() {

        if (configManager == null) {

            synchronized(ConfigManager.class) {

                if (configManager == null) {
                    configManager = new ConfigManager();
                }
            }

        }
        return configManager;
    }

    /**
     * Get String property value
     *
     * @param key
     * @return {@link String}
     * @throws Exception
     */
    public String getString(String key) {
        String val = System.getProperty(key);
        return !StringUtils.isEmpty(val) ? val : config.getString(key);
    }


    /**
     * Get Int property value
     *
     * @param key
     * @return {@link Integer}
     * @throws Exception
     */
    public Integer getInt(String key) {
        String val = System.getProperty(key);
        return !StringUtils.isEmpty(val) ? Integer.parseInt(val.trim()) : config.getInt(key);
    }

    /**
     * Get Int property value
     *
     * @param key
     * @return {@link Boolean}
     * @throws Exception
     */
    public Boolean getBoolean(String key) {
        String val = System.getProperty(key);
        return !StringUtils.isEmpty(val) ? Boolean.parseBoolean(val.trim()) : config.getBoolean(key);
    }

    /**
     * Sets property value of default configuration fields
     *
     * @param key
     * @param value
     */
    public void setPropertyDefault(String key, Object value) {

        try {
            if (defaultConfig.containsKey(key)) {
                defaultConfig.setProperty(key, value);
                defaultConfig.save();
            }

        } catch (ConfigurationException e) {
            e.printStackTrace();
        }

    }

    /**
     * Sets value of user defined config fields
     *
     * @param key
     * @param value
     */
    public void setPropertyUserConfig(String key, Object value) {

        try {

            if (overrideConfig.containsKey(key)) {
                overrideConfig.setProperty(key, value);
                overrideConfig.save();
            }

        } catch (ConfigurationException e) {
            e.printStackTrace();
        }

    }

}
