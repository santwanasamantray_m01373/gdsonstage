package com.myntra.utils.config;
/* Created by Santwana.Samantray on 04/12/20 */

public enum ConfigProperties {

    HOST("host"), PROTOCOL("protocol"), GRPCPORT("grpcPort"), ENVIRONMENT("environment"),

    DOCKINS_URL("dockinsurl"), IS_DOCKINS_ENABLED("isDockinsEnabled"), DOCK_ENV_NAME("dockEnvName"),

    TEST_DATEFORMAT("test.dateformat"), IS_DATABASE_ENABLED("database.enabled"), PORT_UPLOAD("port.upload"),

    RETRY_COUNT("retry.count"), JDBC_DRIVERS("jdbc.drivers"), JDBC_URL("jdbc.url"),

    JDBC_USERNAME("jdbc.username"), JDBC_PASSWORD("jdbc.password"), UI_WEBDRIVER_GRID("ui.webdriver.grid"),

    UI_MOBILE_DEVICEID("ui.mobile.deviceid"), UI_BASE_URL("ui.base.url"), UI_DRIVER_TYPE("ui.driver.type"),

    UI_TESTS_ENABLED("ui.tests.enabled"), UI_LOCATORS_BASE_DIRECTORY("ui.locators.base_dir"),

    UI_SERVICE_NAME("ui.service.name"), UI_CHANNEL("channel"), UI_SCREENSHOTS_DIR("ui.screenshots.directory"),

    DEVICE("device"), ROTATE_LANDSCAPE("rotate.device.landscape"), CHANNEL("channel"), BROWSER("browser"),

    UI_MAX_WAIT_TIMEOUT("ui.wait.timeout"), USE_EMULATION("use.emulation"), IS_CI_CD("is.cicd"),

    CI_CD_DB_HOST("cicd.db.host"), DB_MAX_POOL_SIZE("db.maxpool.size"), SLACK_HOOK_BASE_URL("slackHookBaseUrl"),
    SLACK_WEBHOOK_URL("slackWebHookUrl"),

    ALLURE_FOLDER("allureResultsFolder"), SLACK_POST_MESSAGE("slackPostMsgUrl"), SLACK_TOKEN("slackToken"),
    SLACK_CHANNEL("slackChannel"),

    SEND_TO_SLACK("sendToSlack"), SLACK_VIA_WEBHOOK_OR_AUTH("slackViaWebhookOrAuth"),
    DASHBOARD_DATA_PUSH_ENABLED("dashboard.enabled"), LOG_CURL_REQUEST("logCurlRequest");

    private final String key;

    ConfigProperties(String key) {
        this.key = key;
    }

    public String getKey() {
        return this.key;
    }

}