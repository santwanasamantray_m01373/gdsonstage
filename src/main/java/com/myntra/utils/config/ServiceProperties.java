package com.myntra.utils.config;
/* Created by Santwana.Samantray on 04/12/20 */

public class ServiceProperties {
   // IDEA("/src/test/resources/idea.sql"), IDEA2("/src/test/resources/idea2.sql"), OMSDB("myntra_oms");

    private final String service;

    public ServiceProperties(String service) {
        this.service = service;
    }

    public String getService() {
        return this.service;
    }

}
